import java.beans.EventHandler;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.event.EventHandler;

public class Wee9part1 extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
	// 1. Create & configure user interface controls
	//------------------------------------------------------------------	
		//Name Label
		Label nameLabel = new Label("Enter Your Name");
		
		//Name Textbox
		TextField nameTextBox = new TextField();
		
		//Button
		Button goButton = new Button();
		goButton.setText("Click Me!");
		
		//button click handler
		
		
		goButton.setOnAction(new EventHandler<ActionEvent>() {
		    @Override
		    public void handle(ActionEvent e) {
		        // Logic for what should happen when you push button
	    			
		    		// ---------------------------------------
		    		// Expected output: 
		    		// When person presses button, output HELLO ____
		    		// ____ = whatever they typed in the box
		    		// ---------------------------------------
		    	
		    		// 1. Get whatever they typed in the box
		    	
		    		// 2. Display "HELLO ____" in the console
		    		
		    		
		    }
		});
	//------------------------------------------------------------------
		
		// 2. Make a layout manager
	//------------------------------------------------------------------	
		VBox root = new VBox();
		root.setSpacing(10);
		
	//------------------------------------------------------------------	
		// 3. Add controls to the layout manager
	//------------------------------------------------------------------
		//Ad controls in the same order you want them to appear
		root.getChildren().add(nameLabel);
		root.getChildren().add(nameTextBox);
		root.getChildren().add(goButton);
	//------------------------------------------------------------------
		// 4. Add layout manager to scene
		// 5. Add scene to a stage
	//------------------------------------------------------------------
		primaryStage.setScene(new Scene(root,300,250));
		
		//setting the title bar of the app
		primaryStage.setTitle("Example 01");
		
	//------------------------------------------------------------------
		// 6. Show the app
	//------------------------------------------------------------------
		primaryStage.show();
	//------------------------------------------------------------------
	}

}
